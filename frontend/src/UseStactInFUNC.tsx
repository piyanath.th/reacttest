import React, { useEffect, useState } from 'react'
import './App.css';



function UseStactInFUNC() {
  const [message,setMessage] = useState("I'm Piyanath");

  useEffect(() => {
    fetch('http://localhost:3000/courses/hello')
    .then(res => res.json())
    .then(
      obj => {setMessage(obj.message)}
    );
  }, [])
  return (
    <div>
      {message}
    </div>
  )
}
export default UseStactInFUNC;