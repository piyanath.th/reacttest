import React, { Component } from 'react';

import './App.css';





// function App() {
//   return (
//     <div className="App">
//       hello
//     </div>
//   );
// }






type AppState = {
  message?: string;
};

class App extends Component<{},AppState> {
  state: AppState = {
    message: '555',
  }

  componentDidMount(){
    fetch('http://localhost:3000/courses/hello').then(res => res.json()).then(
      obj => this.setState({message: obj.message})
    )
  }
  render() {
    return (
      <div>
        {this.state.message}
      </div>
    )
  }
}





























export default App;
