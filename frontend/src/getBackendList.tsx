import { link } from "fs";
import React, { useEffect, useState } from "react";
import "./App.css";

function GetBackendList() {
  const [courses, setCourses] = useState<any[]>([]);

  useEffect(() => {
    fetch("http://localhost:3000/courses/getcouse")
      .then((res) => res.json())
      .then((courses) => {
        setCourses(courses);
      });
  }, []);
  return (
    <div>
      <ul>
        {courses.map((item) => {
          return (
            <li key={item.id}>
              {item.id} {item.title}
            </li>
          );
        })}
      </ul>
    </div>
  );
}
export default GetBackendList;
