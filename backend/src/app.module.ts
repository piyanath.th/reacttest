import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CoureseController } from './courses.controller';

@Module({
  imports: [],
  controllers: [AppController,CoureseController],
  providers: [AppService],
})
export class AppModule {}
